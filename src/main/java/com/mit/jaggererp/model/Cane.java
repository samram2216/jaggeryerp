package com.mit.jaggererp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class Cane {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Integer caneId;
	
	@Column
	private String estHarvestDate;
	
	@Column
	private Integer rowsDistance;
	
	@Column
	private Integer caneDistance;
	
	@Column
	private String  plantationType; //horizontal,vertical,circular
	
	@Column
	private String  PlantationDate;
	
	
	@Column
	private Integer totalPlantation;
	
	@OneToOne
	private CaneVariety caneVariety;

	public Integer getCaneId() {
		return caneId;
	}

	public void setCaneId(Integer caneId) {
		this.caneId = caneId;
	}

	public String getEstHarvestDate() {
		return estHarvestDate;
	}

	public void setEstHarvestDate(String estHarvestDate) {
		this.estHarvestDate = estHarvestDate;
	}

	public Integer getRowsDistance() {
		return rowsDistance;
	}

	public void setRowsDistance(Integer rowsDistance) {
		this.rowsDistance = rowsDistance;
	}

	public Integer getCaneDistance() {
		return caneDistance;
	}

	public void setCaneDistance(Integer caneDistance) {
		this.caneDistance = caneDistance;
	}

	public String getPlantationType() {
		return plantationType;
	}

	public void setPlantationType(String plantationType) {
		this.plantationType = plantationType;
	}

	public String getPlantationDate() {
		return PlantationDate;
	}

	public void setPlantationDate(String plantationDate) {
		PlantationDate = plantationDate;
	}

	public Integer getTotalPlantation() {
		return totalPlantation;
	}

	public void setTotalPlantation(Integer totalPlantation) {
		this.totalPlantation = totalPlantation;
	}

	public CaneVariety getCaneVariety() {
		return caneVariety;
	}

	public void setCaneVariety(CaneVariety caneVariety) {
		this.caneVariety = caneVariety;
	}
	
	
	
	
}

