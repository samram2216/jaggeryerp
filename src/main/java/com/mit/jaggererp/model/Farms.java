package com.mit.jaggererp.model;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.boot.autoconfigure.web.ResourceProperties.Strategy;


@Entity
public class Farms {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Integer farmId;
	
	@Column
	private String gatNo;
	
	@Column
	private String doc7_12Path;
	
	@Column
	private String doc8APath;
	
	@Column
	private Integer totalArea;
	
	@Column
	private Integer hector;
	
	@Column
	private Integer r;
	
	@Column
	private String soilType;
	
	@Column
	private String irrigationType;
	
	@Column
	private String irrigationSource;
	
	@Column
	private String fertilizer;
	
	@Column
	private String distanceToFarm;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "villageId")
	private Village village;
	
	@ManyToOne
	@JoinColumn(name = "farmerId")
	private Farmers farmers;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="caneId")
	private Cane cane;


	public Integer getFarmId() {
		return farmId;
	}


	public void setFarmId(Integer farmId) {
		this.farmId = farmId;
	}


	public String getGatNo() {
		return gatNo;
	}


	public void setGatNo(String gatNo) {
		this.gatNo = gatNo;
	}


	public String getDoc7_12Path() {
		return doc7_12Path;
	}


	public void setDoc7_12Path(String doc7_12Path) {
		this.doc7_12Path = doc7_12Path;
	}


	public String getDoc8APath() {
		return doc8APath;
	}


	public void setDoc8APath(String doc8aPath) {
		doc8APath = doc8aPath;
	}


	public Integer getTotalArea() {
		return totalArea;
	}


	public void setTotalArea(Integer totalArea) {
		this.totalArea = totalArea;
	}


	public Integer getHector() {
		return hector;
	}


	public void setHector(Integer hector) {
		this.hector = hector;
	}


	public Integer getR() {
		return r;
	}


	public void setR(Integer r) {
		this.r = r;
	}


	public String getSoilType() {
		return soilType;
	}


	public void setSoilType(String soilType) {
		this.soilType = soilType;
	}


	public String getIrrigationType() {
		return irrigationType;
	}


	public void setIrrigationType(String irrigationType) {
		this.irrigationType = irrigationType;
	}


	public String getIrrigationSource() {
		return irrigationSource;
	}


	public void setIrrigationSource(String irrigationSource) {
		this.irrigationSource = irrigationSource;
	}


	public String getFertilizer() {
		return fertilizer;
	}


	public void setFertilizer(String fertilizer) {
		this.fertilizer = fertilizer;
	}


	public String getDistanceToFarm() {
		return distanceToFarm;
	}


	public void setDistanceToFarm(String distanceToFarm) {
		this.distanceToFarm = distanceToFarm;
	}


	public Village getVillage() {
		return village;
	}


	public void setVillage(Village village) {
		this.village = village;
	}


	public Farmers getFarmers() {
		return farmers;
	}


	public void setFarmers(Farmers farmers) {
		this.farmers = farmers;
	}


	public Cane getCane() {
		return cane;
	}


	public void setCane(Cane cane) {
		this.cane = cane;
	}
	
	
	
}

