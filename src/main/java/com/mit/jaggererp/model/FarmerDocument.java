package com.mit.jaggererp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class FarmerDocument {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer docId;
	
	@Column
	private String docName;
	
	@Column
	private String docNo;
	
	@Column
	private String docDesc;
	
	@ManyToOne
	@JoinColumn(name = "farmerId")
	private Farmers farmers;
	
	public Integer getDocId() {
		return docId;
	}
	public void setDocId(Integer docId) {
		this.docId = docId;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getDocDesc() {
		return docDesc;
	}
	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}
	
	
	
	
}
