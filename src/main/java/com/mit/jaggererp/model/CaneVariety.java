package com.mit.jaggererp.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class CaneVariety {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column
	private Integer caneId;
	
	@Column
	private String caneVarietyType;
	
	@Column
	private String caneVarietyDesc;
	
	@Column
	private Integer createdBy;
	
	@Column
	private Date createdDate;
	
	@Column
	private Integer updatedBy;
	
	@Column
	private Date updatedDate;
	
	@Column
	private String status;
	
	@Column
	public Integer getCaneId() {
		return caneId;
	}
	public void setCaneId(Integer caneId) {
		this.caneId = caneId;
	}
	public String getCaneVarietyType() {
		return caneVarietyType;
	}
	public void setCaneVarietyType(String caneVarietyType) {
		this.caneVarietyType = caneVarietyType;
	}
	public String getCaneVarietyDesc() {
		return caneVarietyDesc;
	}
	public void setCaneVarietyDesc(String caneVarietyDesc) {
		this.caneVarietyDesc = caneVarietyDesc;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}





