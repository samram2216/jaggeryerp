package com.mit.jaggererp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class JaggeryErpApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	
	@GetMapping("/")
	String home1() {
		return "Spring is here!";
	}

	@GetMapping("/")
	String home2() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(JaggeryErpApplication.class, args);
	}
}